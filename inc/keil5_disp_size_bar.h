/**
 * @file 文件名： keil5_disp_size_bar.h
 * @author 作者： ageek_nikola  邮箱：(2503865771@qq.com)
 * @brief  功能介绍：
 * @version 版本：1.0
 * @date 日期：2023-07-28
 * 
 * gitee开源地址:https://gitee.com/nikolan/keil5_disp_size_bar
 * 
 */
#ifndef KEIL5_DISP_SIZE_BAR_H
#define KEIL5_DISP_SIZE_BAR_H

#ifdef  __cplusplus
extern "C" {
#endif


#define APP_NAME "Keil5_disp_size_bar V0.5"


#if USED_XLSXWRITER_LIB

#include "xlsxwriter.h"

#endif

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <errno.h>







// 枚举2种存储类型 一种是ram内存存储，一种是flash存储
typedef enum
{
    _ram,
    _flash
} storage_type_t;

// 定义存储结构体
typedef struct storage
{
    storage_type_t type;    // 存储类型
    char name[50] ;         //在map执行段的名称
    uint64_t base_addr;     // 基地址
    uint64_t used_size;     // 已使用大小
    uint64_t available_max; // 可使用最大
} storage_t;

typedef enum mcu_core
{
    _8051,//8051 51单片机
    _arm//arm内核 单片机

} mcu_core_t;

// 定义工程信息结构体
typedef struct project_info
{
    
    char mcu_name[30];//主控名称
    char core_name[30];//内核名称
    char target_name[30];//工程名称
    mcu_core_t core;//主控类型
    storage_t iram; // 默认内部ram
    storage_t irom; // 默认内部rom
    storage_t xram; // 外部ram
    uint8_t ram_conut;//ram个数
    uint8_t flash_count;//flash个数
} project_info_t;


// 定义文件占比结构体
typedef struct map_info_item
{
    

char Object_name[30];//文件名
float flash_percent;//已经使用的flash占比
float ram_percent;//已经使用的ram的占比
uint64_t flash_size;//flash大小
uint64_t ram_size;//ram大小
uint64_t Code;//代码段
uint64_t inc_data;//数据段包含在代码段内了
uint64_t RO_data;//只读数据段
uint64_t RW_data;//可读可写数据段
uint64_t ZI_data;//
uint64_t Debug;//



} map_info_item_t;

typedef struct info_node {
    map_info_item_t data;//数据区
    struct info_node* next;//指针区
} info_node_t;//信息结点


#if USED_XLSXWRITER_LIB

typedef struct xlsx_data {
    lxw_workbook     *workbook;
    lxw_worksheet    *worksheet_ram_percent;
    lxw_worksheet    *worksheet_flash_percent;

    lxw_chart *piechart_ram;
    lxw_chart *piechart_flash;    

}xlsx_data_t;

#endif


// 通用API
void print_erro_info(const char *msg);
void print_msg(const char *tag, const char *format, ...);             // 打印消息，在程序运行出问题或者调试时输出消息
bool find_path(const char *dir_path, const char *file_key_word, char *file_path); // 递归查找文件
int64_t sreach_string(FILE *fp, char *sub_str, int seek, uint32_t offset);//文件内找子串位置

// 解析工程文件API
bool parse_keil_porject(char *path); // 解析工程文件定位到工程信息结构体变量


// 解析map信息API
bool parse_keil_map(char *path);// 解析map文件定位到执行段文本
bool parse_file_proportion(char *file_name);// 解析文件占比

//单链表结构不过只涉及按大小来排序插入和清除
info_node_t* create_node(map_info_item_t item);//创建新结点
void insert_in_sorted_order_by_flash_size(info_node_t** head, info_node_t* new_node);//按flash_size降序插入到链表
void insert_in_sorted_order_by_ram_size(info_node_t** head, info_node_t* new_node) ;//按ram_size降序插入到链表
void free_linked_list(info_node_t** head);//释放链表内存

bool parse_file_proportion_and_export_csv_xlsx(char *path); // 把占比信息导出按flash和ram大小插入到不同的链表
bool export_csv(char *path,info_node_t *list_ram,info_node_t *list_flash);//出到csv文件,使用excel打开可以画图

#if USED_XLSXWRITER_LIB
bool export_xlsx(char *path,info_node_t *list_ram,info_node_t *list_flash); // 把占比信息到xlsx文件,使用excel打开就有画好的图
#endif

// 解析8051的m51信息API
bool parse_keil_m51(char *path);             // 解析m51文件占用信息
void prtint_percentage_bar_8051(storage_t storage);

// 输出API
void prtint_percentage_bar(storage_t storage); // 打印进度条


#ifdef  __cplusplus
}
#endif

#endif // KEIL5_DISP_SIZE_BAR_H